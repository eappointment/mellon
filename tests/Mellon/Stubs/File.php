<?php

declare(strict_types=1);

namespace BO\Mellon\Tests\Stubs;

use Psr\Http\Message\StreamInterface;
use Psr\Http\Message\UploadedFileInterface;

class File implements UploadedFileInterface
{
    protected $data = [];

    public function __construct(array $data)
    {
        $this->data = $data;
    }

    public function getError(): int
    {
        return $this->data['error'] ?? UPLOAD_ERR_NO_FILE;
    }

    public function getSize(): ?int
    {
        return $this->data['size'] ?? 0;
    }

    public function getClientMediaType(): ?string
    {
        return $this->data['type'] ?? '';
    }

    public function getStream(): StreamInterface
    {
        return new Stream();
    }

    public function moveTo(string $targetPath): void
    {
    }

    public function getClientFilename(): ?string
    {
        return $this->data['tmp_name'] ?? '';
    }
}
