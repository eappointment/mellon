<?php

declare(strict_types=1);

namespace BO\Mellon\Tests;

use BO\Mellon\Validator;
use PHPUnit\Framework\TestCase;

class DateTest extends TestCase
{
    public function testIsDate(): void
    {
        self::assertFalse(Validator::value('2002-02-20')->isDate('Y-m-d')->hasFailed());
        self::assertFalse(Validator::value(123456789)->isDate('U')->hasFailed());
        self::assertTrue(Validator::value(null)->isDate('U')->hasFailed());
        self::assertTrue(Validator::value('Winter')->isDate('M')->hasFailed());
    }
}
