<?php
/**
 * @package Mellon
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

namespace BO\Mellon\Tests;

use BO\Mellon\Validator;
use PHPUnit\Framework\TestCase;

class BoolTest extends TestCase
{
    public function testBoolean(): void
    {
        self::assertTrue(Validator::value(null)->isBool()->hasFailed());

        $trueValues = array(
            true,
            'on',
            'yes',
            1
        );
        foreach ($trueValues as $value) {
            $this->assertTrue(
                Validator::value($value)->isBool()->getValue(),
                var_export($value, true) . ' should be true'
            );
        }
        $falseValues = array(
            false,
            'no',
            '',
            0,
            'off',
        );
        foreach ($falseValues as $value) {
            $this->assertFalse(
                Validator::value($value)->isBool()->getValue(),
                var_export($value, true) . ' should be false'
            );
        }
        $this->assertTrue(
            Validator::value('dummy')->isBool()->hasFailed(),
            'unknown value for boolean should not validate'
        );
    }
}
