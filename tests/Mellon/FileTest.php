<?php

declare(strict_types=1);

namespace BO\Mellon\Tests;

use BO\Mellon\Validator;

use BO\Mellon\Tests\Stubs\File;

use PHPUnit\Framework\TestCase;

class FileTest extends TestCase
{
    protected $validator;

    public function setUp(): void
    {
        $files['goodfile'] = [
            'tmp_name' => 'README.md',
            'type'     => 'text/markdown',
            'size'     => 321000,
            'error'    => 0,
        ];
        $files['failedfile'] = [
            'tmp_name' => 'README.md',
            'type'     => 'text/markdown',
            'size'     => 321000,
            'error'    => UPLOAD_ERR_PARTIAL,
        ];
        $files['corruptedfile'] = [
            'tmp_name' => '',
            'type'     => '',
            'size'     => 0,
            'error'    => UPLOAD_ERR_CANT_WRITE,
        ];
        $files['wrongfile'] = [
            'tmp_name' => 'virus.exe',
            'type'     => 'application/octet-stream',
            'size'     => 321000,
            'error'    => 0,
        ];
        $files['bigfile'] = [
            'tmp_name' => 'photo.bmp',
            'type'     => 'image/x-ms-bmp',
            'size'     => 4321000,
            'error'    => 0,
        ];

        $this->validator = new Validator([], null, $files);
    }

    public function testGetFile(): void
    {
        $goodfile = [
            'tmp_name' => 'README.md',
            'type'     => 'text/markdown',
            'size'     => 321000,
            'error'    => 0,
        ];
        $collected = [
            0 => [
                'tmp_name' => 'cv.doc',
                'type'     => 'application/msword',
                'size'     => 321000,
                'error'    => 0,
            ],
            1 => [
                'tmp_name' => 'references.ppt',
                'type'     => 'application/vnd.ms-powerpoint',
                'size'     => 4321000,
                'error'    => 0,
            ],
        ];

        self::assertFalse($this->validator->getFile('goodfile')->hasFailed());
        self::assertFalse($this->validator->getFile('goodfile')->isOfType('document')->hasMaxSize(500)->hasFailed());
        self::assertFalse(
            (new Validator([], null, ['goodfile' => new File($goodfile)])) // $value instanceof UploadedFileInterface
                ->getFile('goodfile')
                ->isOfType('document')
                ->hasMaxSize(500)
                ->hasFailed()
        );
        self::assertSame($goodfile, $this->validator->getFile('goodfile')->getValue());
        self::assertTrue($this->validator->getFile('failedfile')->hasFailed());
        self::assertTrue($this->validator->getFile('corruptedfile')->hasFailed());
        self::assertTrue((new Validator([], null, []))->getFile('somefile')->hasFailed());
        self::assertTrue((new Validator([], null, ['somefile' => '']))->getFile('somefile')->hasFailed());
        self::assertTrue((new Validator([], null, ['application' => $collected]))->getFile('application')->hasFailed());
    }

    public function testIsOfType(): void
    {
        self::assertTrue($this->validator->getFile('wrongfile')->isOfType('document')->hasFailed());
        self::assertFalse($this->validator->getFile('wrongfile')->hasMaxSize(500)->hasFailed());
    }

    public function testHasMaxSize(): void
    {
        self::assertFalse($this->validator->getFile('bigfile')->isOfType('image')->hasFailed());
        self::assertTrue($this->validator->getFile('bigfile')->hasMaxSize(500)->hasFailed());
    }
}
