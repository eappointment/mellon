<?php
/**
 * @package Mellon
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

namespace BO\Mellon\Tests;

use BO\Mellon\Collection;
use BO\Mellon\Exception;
use BO\Mellon\Validator;
use PHPUnit\Framework\TestCase;
use stdClass;

class CollectionTest extends TestCase
{
    public function testUsage(): void
    {
        (new Validator([
            'name' => 'test',
            'phone' => '1234',
        ]))->makeInstance();

        $collection = Validator::collection([
            'name' => Validator::param('name')
                ->isString()
                ->setDefault('dummy'),
            'phone' => Validator::param('phone')
                ->isNumber()
                ->setDefault('11'),
            'remark' => Validator::param('remark')
                ->replace(null, '')
                ->isString(),
        ]);
        $this->assertFalse($collection->hasFailed(), 'Collection usage should validate');
        $form = $collection->getValues();
        $this->assertArrayHasKey('name', $form, 'Collection should return name');
    }

    public function testGetValid(): void
    {
        $collection = Validator::collection([
            'name'  => Validator::value('John')->isString()->isShorterThan(100),
            'email' => Validator::value('developer@example.com')->isMail(),
        ]);

        self::assertSame('John', $collection->getValid('name')->getValue());
        self::assertNull($collection->getValid('birthday'));
        self::assertSame('', (string) $collection->getValid('birthday'));
    }

    public function testFail(): void
    {
        $collection = Validator::collection(array(
            'name' => Validator::value('test')
                ->isNumber()
        ));
        $this->assertTrue($collection->hasFailed(), 'Collection should fail');
    }

    public function testRecursive(): void
    {
        $collection = Validator::collection(array(
            'sub' => array(
                'name' => Validator::value('test')
                    ->isNumber()
            )
        ));
        $this->assertTrue($collection->hasFailed(), 'Recursive Collection should fail');
    }

    public function testValidOnly(): void
    {
        $this->expectException('\BO\Mellon\Exception');
        Validator::collection(array(
            'name' => Validator::value('test')
        ));
    }

    public function testMessages(): void
    {
        $collection = Validator::collection(array(
            'name' => Validator::value('test')->isNumber('testMessage'),
            'sub' => array(
                'phone' => Validator::value('1234')->isNumber(),
            ),
        ));
        $messages = $collection->getStatus(null, true);
        $this->assertEquals('testMessage', $messages['name']['messages'][0], 'Failed should return testMessage');
        $this->assertEquals('test', $messages['name']['_unvalidated'], 'original value should be in status');

        $collection = new Collection([]);
        $collection->addValid(Validator::value(null)->isDeclared());
        $collection->addValid(Validator::value(['good', 'suspicious'])->isArray()->isDevoidOf(['suspicious']));

        self::assertTrue($collection->hasFailed());
        self::assertCount(2, $collection->getStatus());
    }

    public function testValidatedAction(): void
    {
        $backGroundTask = function ($value) {
            self::assertSame('task', $value);
        };

        $dataBuild = new stdClass();
        $dataBuild->id = 123;
        $dataBuild->loadAdditional = function ($id) use ($dataBuild) {
            $dataBuild->additional = ['id' => $id, 'other' => 'data'];
        };

        $securityTask = function ($forbidden) {
            throw new \Exception("Someone requested $forbidden");
        };

        $collection = new Collection([]);
        $collection->validatedAction(
            Validator::value('task', 'task')->isString()->isRequired(),
            $backGroundTask
        );
        $collection->validatedAction(
            Validator::value($dataBuild->id, 'id')->isNumber()->isGreaterThan(0),
            $dataBuild->loadAdditional
        );

        self::assertFalse($collection->hasFailed());
        self::assertObjectHasProperty('additional', $dataBuild);

        $collection->validatedAction(
            Validator::value('access')->isUrl()->isNotEqualTo('access'),
            $securityTask
        );

        self::assertTrue($collection->hasFailed());
    }
}
