<?php
/**
 * @package Mellon
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

namespace BO\Mellon\Tests;

use BO\Mellon\Validator;
use PHPUnit\Framework\TestCase;

class ValidationTest extends TestCase
{
    public function testAssert(): void
    {
        self::assertTrue(Validator::value(true)->isBool()->assertValid()->getValue());

        $this->expectException("\BO\Mellon\Failure\Exception");
        Validator::value("test")->isBool()->assertValid();
    }

    public function testMissingFunction(): void
    {
        $this->expectException("\BO\Mellon\Exception");
        Validator::value("test")->isANotExistingFunction();
    }

    public function testInvalidFunction(): void
    {
        $this->expectException("\BO\Mellon\Exception");
        Validator::value("test")->isaninvalidfunction();
    }

    public function testInvalidValidationFunction(): void
    {
        $this->expectException("\BO\Mellon\Exception");
        Validator::value("test")->isString()->isANotExistingFunction();
    }

    public function testUndefinedFunction(): void
    {
        $this->expectException("\BO\Mellon\Exception");
        Validator::value("test")->isString()->aninvalidfunction();
    }
}
