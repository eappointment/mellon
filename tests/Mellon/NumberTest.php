<?php
/**
 * @package Mellon
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

namespace BO\Mellon\Tests;

use BO\Mellon\Validator;
use PHPUnit\Framework\TestCase;

class NumberTest extends TestCase
{
    public function testNumber(): void
    {
        $value = "12345";
        $invalidValue = Validator::value($value);
        $validValue = $invalidValue->isNumber();
        $this->assertEquals($value, "$validValue");
        $value = "abc";
        $invalidValue = Validator::value($value);
        $validValue = $invalidValue->isNumber();
        $this->assertNotEquals($value, "$validValue");

        self::assertTrue(Validator::value(null)->isNumber()->hasFailed());
        self::assertTrue(Validator::value(false)->isNumber()->hasFailed());
        self::assertTrue(Validator::value(true)->isNumber()->hasFailed());
        self::assertFalse(Validator::value(123)->isNumber()->hasFailed());
        self::assertFalse(Validator::value('123')->isNumber()->hasFailed());

        self::assertTrue(Validator::value('1')->isNumber()->isGreaterThan(1)->hasFailed());
        self::assertTrue(Validator::value('1')->isNumber()->isGreaterThan(3)->hasFailed());
        self::assertFalse(Validator::value('2')->isNumber()->isGreaterThan(1)->hasFailed());
        self::assertTrue(Validator::value('1')->isNumber()->isGreaterEqualThan(2)->hasFailed());
        self::assertFalse(Validator::value('1')->isNumber()->isGreaterEqualThan(1)->hasFailed());
        self::assertFalse(Validator::value('2')->isNumber()->isGreaterEqualThan(1)->hasFailed());

        self::assertTrue(Validator::value('1')->isNumber()->isLowerThan(1)->hasFailed());
        self::assertFalse(Validator::value('1')->isNumber()->isLowerThan(2)->hasFailed());
        self::assertTrue(Validator::value('2')->isNumber()->isLowerEqualThan(1)->hasFailed());
        self::assertFalse(Validator::value('2')->isNumber()->isLowerEqualThan(2)->hasFailed());
        self::assertFalse(Validator::value('2')->isNumber()->isLowerEqualThan(3)->hasFailed());

        self::assertTrue(Validator::value(null)->isNumber()->isLowerThan(1)->hasFailed());
        self::assertTrue(Validator::value(false)->isNumber()->isLowerThan(1)->hasFailed());
        self::assertTrue(Validator::value(true)->isNumber()->isLowerThan(1)->hasFailed());
    }
}
