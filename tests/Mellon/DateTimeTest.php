<?php

declare(strict_types=1);

namespace BO\Mellon\Tests;

use DateTime;
use BO\Mellon\Validator;
use PHPUnit\Framework\TestCase;

class DateTimeTest extends TestCase
{
    public function testIsDatetime(): void
    {
        self::assertTrue(Validator::value(null)->isDateTime('U')->hasFailed());
        self::assertTrue(Validator::value('2002-02-20-12-34-56')->isDateTime('U')->hasFailed());
        self::assertFalse(Validator::value('2002-02-20-12-34-56')->isDateTime('Y-m-d-H-i-s')->hasFailed());
        self::assertFalse(Validator::value((new DateTime())->format(DATE_ATOM))->isDateTime()->hasFailed());
        self::assertFalse(Validator::value(time())->isDateTime('U')->hasFailed());
        self::assertTrue(Validator::value(time())->isDateTime('U')->isOldEnough(18)->hasFailed());
        self::assertFalse(Validator::value(123456)->isDateTime('U')->isOldEnough(18)->hasFailed());
    }
}
