<?php
/**
 * @package Mellon
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

namespace BO\Mellon\Tests;

use BO\Mellon\Validator;
use PHPUnit\Framework\TestCase;

class JsonTest extends TestCase
{

    public function testJson(): void
    {
        $value = '{"test":"value"}';
        $valid = Validator::value($value)->isJson()->setDefault(array('Test'))->setDefaultJson('["Test"]');
        $this->assertFalse(
            $valid->hasFailed(),
            "JSON '$value' should validate"
        );
        $this->assertEquals($value, (string)$valid);
        $this->assertEquals(array('test' => 'value'), $valid->getValue());

        $value = "abc";
        $valid = Validator::value($value)->isJson()->setDefault(array('Test'))->setDefaultJson('["Test"]');
        $this->assertTrue(
            $valid->hasFailed(),
            "JSON '$value' should not validate"
        );
        $this->assertEquals(array('Test'), $valid->getValue());
        $this->assertEquals('["Test"]', (string)$valid);

        self::assertTrue(Validator::value(null)->isJson()->hasFailed());
        self::assertSame(
            "Validation failed: Json: Empty\n",
            (string) Validator::value(null)->isJson()->getMessages()
        );
        self::assertTrue(Validator::value('')->isJson()->hasFailed());
        self::assertSame(
            "Validation failed: Json: Empty string\n",
            (string) Validator::value('')->isJson()->getMessages()
        );
    }
}
