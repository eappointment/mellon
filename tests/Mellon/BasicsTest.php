<?php
/**
 * @package Mellon
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

namespace BO\Mellon\Tests;

use BO\Mellon\Unvalidated;
use BO\Mellon\Validator;
use BO\Mellon\Valid;
use BO\Mellon\ValidNumber;
use PHPUnit\Framework\TestCase;

class BasicsTest extends TestCase
{
    public function testBasic(): void
    {
        $validTest = Validator::param("test")->isBool();
        $this->assertTrue($validTest instanceof Valid);
    }

    public function testDefaultvalue(): void
    {
        $valid = new Valid("123");
        $this->assertEquals(
            null,
            $valid->getValue(),
            "Unvalidated parameters without a default value should return NULL"
        );
        $valid->setDefault('456');
        $this->assertEquals('456', $valid->getValue(), "Unvalidated parameters should return the default value");
    }

    public function testRequired(): void
    {
        $valid = Validator::value('abc')->isRequired("Value abc should not fail");
        $this->assertFalse($valid->hasFailed(), "Should not show");
        $valid = Validator::value('')->isRequired("Empty value should fail");
        $this->assertTrue($valid->hasFailed(), $valid->getMessages());
    }

    public function testStdin(): void
    {
        $valid = Validator::input()->isDeclared();
        $this->assertTrue($valid instanceof Valid);
        Validator::resetInstance();
        $validator = new Validator([], "InputBody");
        $this->assertEquals("InputBody", $validator->getInput()->isString()->getValue());
        $validator->makeInstance();
        $valid = Validator::input()->isString();
        $this->assertEquals("InputBody", $valid->getValue());
    }

    public function testUnvalidated()
    {
        $value = Validator::value('123');
        $this->assertInstanceOf(
            Unvalidated::class,
            $value,
            "Parameter should not be of class Valid if no validation happened"
        );

        $this->expectException('\BO\Mellon\Exception');
        $this->expectExceptionMessage('Parameters should validate first.');
        $value->getValue();
    }

    public function testReplace()
    {
        $value = Validator::value(null);
        self::assertTrue($value->isString()->hasFailed(), 'NULL should fail when a string is expected');
        self::assertFalse(
            $value->replace(null, '')->isString()->hasFailed(),
            'The not existing parameter should be handled as an empty string an not fail when a string is expected'
        );

        $value = Validator::value('0123456789');
        self::assertTrue(
            $value->replace(null, '')->isString()->isShorterThan(8)->hasFailed(),
            'normal String checks should fail as expected'
        );

        $this->expectException(\ArgumentCountError::class);
        $value->replace(null)->isString()->hasFailed();
    }

    public function testSetCallback()
    {
        $callbackCheck = function ($obj) {
            self::assertInstanceOf(ValidNumber::class, $obj);
        };

        self::assertFalse(Validator::value('123')->setCallback($callbackCheck)->isNumber()->hasFailed());
    }

    public function testReturn()
    {
        $this->assertEquals(
            'abc',
            Validator::value('abc')->isString()->getValue(),
            'getValue did not return string'
        );
        $this->assertEquals(
            'abc',
            (string)Validator::value('abc')->isString(),
            'casting to string did not return string'
        );
        $status = Validator::value('abc')->isString()->getStatus();
        $this->assertArrayHasKey('failed', $status, 'No status failed found');
        $this->assertArrayHasKey('value', $status, 'No status value found');
        $this->assertArrayHasKey('messages', $status, 'No status messages found');
    }

    public function testConstructFail()
    {
        $this->expectException('\BO\Mellon\Exception');
        new Validator('123');
    }

    public function testMock()
    {
        Validator::resetInstance();
        $validator = new Validator(['test' => '123']);
        $value = Validator::param('test')->isNumber();
        $this->assertNotEquals('123', "$value");
        $validator->makeInstance();
        $value = Validator::param('test')->isNumber();
        $this->assertEquals('123', "$value");
    }
}
