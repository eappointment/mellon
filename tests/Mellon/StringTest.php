<?php
/**
 * @package Mellon
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

namespace BO\Mellon\Tests;

use BO\Mellon\Validator;
use PHPUnit\Framework\TestCase;

class StringTest extends TestCase
{

    public function testIsString(): void
    {
        $value = "abc";
        $invalidValue = Validator::value($value);
        $validValue = $invalidValue->isString();
        $this->assertEquals($value, "$validValue");

        $value = str_repeat("I am big!!", 7000);
        $invalidValue = Validator::value($value);
        $validValue = $invalidValue->isString()->setDefault('ok');
        $this->assertEquals('ok', $validValue->getValue());

        $this->assertEquals(
            '<h1>',
            Validator::value("<h1>")->isString('', false)->getValue(),
            "HTML Should not be escaped"
        );

        $this->assertEquals(
            '&#60;h1&#62;',
            Validator::value("<h1>")->isString()->getValue(),
            "HTML Should be escaped"
        );

        self::assertFalse(Validator::value(0)->isString()->hasFailed()); // it will be sanitized by default
        self::assertFalse(Validator::value(false)->isString()->hasFailed());
        self::assertFalse(Validator::value(true)->isString()->hasFailed());
        self::assertFalse(Validator::value(12.34)->isString()->hasFailed());

        self::assertSame('0', Validator::value(0)->isString()->getValue());
        self::assertSame('', Validator::value(false)->isString()->getValue());
        self::assertSame('1', Validator::value(true)->isString()->getValue());
        self::assertSame('12.34', Validator::value(12.34)->isString()->getValue());

        self::assertTrue(Validator::value(0)->isString('be strict and set sanitize to false', false)->hasFailed());
        self::assertTrue(Validator::value(false)->isString('be strict and set sanitize to false', false)->hasFailed());
        self::assertTrue(Validator::value(true)->isString('be strict and set sanitize to false', false)->hasFailed());
        self::assertTrue(Validator::value(12.34)->isString('be strict and set sanitize to false', false)->hasFailed());
        self::assertTrue(Validator::value(null)->isString()->hasFailed());
        self::assertTrue(Validator::value(null)->isString()->isRequired()->hasFailed());
        self::assertTrue(
            Validator::value([])
            ->isString()
            ->isSmallerThan(1)
            ->isBiggerThan(1)
            ->isMatchOf('*')
            ->isFreeOf('*')
            ->isShorterThan(1)
            ->isShorterEqualThan(1)
            ->isLongerThan(1)
            ->isLongerEqualThan(1)
            ->hasFailed()
        );

        self::assertNull(Validator::value(null)->isString()->getValue());
        self::assertNull(Validator::value('')->isString()->isRequired()->getValue());
        self::assertNull(Validator::value([])->isString()->getValue());
    }

    public function testIsNotEqualTo(): void
    {
        self::assertTrue(Validator::value('')->isString()->isNotEqualTo('')->hasFailed());
    }

    public function testSize(): void
    {
        $this->assertTrue(
            Validator::value("123456789")->isString()->isSmallerThan(8)->hasFailed(),
            "String of length 9 should not be smaller than 8"
        );
        $this->assertTrue(
            Validator::value("123456789")->isString()->isBiggerThan(10)->hasFailed(),
            "String of length 9 should not be bigger than 10"
        );
    }

    public function testRegex(): void
    {
        $this->assertTrue(
            Validator::value("123456789")->isString()->isMatchOf('/abc/')->hasFailed(),
            "'abc' should not match 123456789"
        );
        $this->assertFalse(
            Validator::value("123456789")->isString()->isMatchOf('/456/')->hasFailed(),
            "'456' should match 123456789"
        );
        $this->assertFalse(
            Validator::value("123456789")->isString()->isFreeOf('/abc/')->hasFailed(),
            "'abc' should not match 123456789"
        );
        $this->assertTrue(
            Validator::value("123456789")->isString()->isFreeOf('/456/')->hasFailed(),
            "'456' should match 123456789"
        );
        $this->assertFalse(
            Validator::value(1)->isString()->isMatchOf('/^(0|1)$/')->hasFailed(),
            "'(0|1)' should match 1"
        );
        $this->assertTrue(
            Validator::value(null)->isString()->isMatchOf('/^(0|1)$/')->hasFailed(),
            "'(0|1)' should not match NULL"
        );
    }

    public function testGetValue(): void
    {
        self::assertSame(
            null,
            Validator::value(null)->isString()->getValue(),
            'Non existing parameter/value should return the default null value.'
        );

        self::assertSame(
            '',
            Validator::value('')->isString()->getValue(),
            'The empty string should get returned.'
        );

        self::assertSame(
            null,
            Validator::value('')->isString()->isRequired()->getValue(),
            'The empty string value should fail the check since a usable value is required => return the default value.'
        );

        self::assertSame(
            null,
            Validator::value(null)->isString()->isLongerThan(0)->getValue(),
            'The non existing parameter/value should return the default null value.'
        );

        self::assertSame(
            null,
            Validator::value('')->isString()->isLongerThan(0)->getValue(),
            'The empty string value should fail the length check and return the default null value.'
        );

        self::assertSame(
            '',
            Validator::value(null)->isString()->isRequired()->setDefault('')->getValue(),
            'The empty string value should fail the requirement check and return the default value: empty string'
        );

        self::assertSame(
            '',
            Validator::value('')->isString()->isLongerThan(0)->setDefault('')->getValue(),
            'The empty string value should fail the length check and return the default value: empty string'
        );
    }

    public function testIsBigger(): void
    {
        self::assertFalse(Validator::value('')->isString()->isBiggerThan(0)->hasFailed()); // 0 > 0 reason to dismiss it
    }

    public function testIsLowerThan(): void
    {
        self::assertFalse(Validator::value('123')->isString()->isSmallerThan(3)->hasFailed()); // 3 < 3, same here
    }

    public function testIsShorterThan(): void
    {
        self::assertTrue(Validator::value('')->isString()->isShorterThan(0)->hasFailed());
        self::assertTrue(Validator::value('1')->isString()->isShorterThan(0)->hasFailed());
        self::assertTrue(Validator::value('1')->isString()->isShorterThan(1)->hasFailed());
        self::assertTrue(Validator::value('12')->isString()->isShorterThan(2)->hasFailed());

        self::assertFalse(Validator::value('1')->isString()->isShorterThan(2)->hasFailed());
        self::assertFalse(Validator::value('12')->isString()->isShorterThan(3)->hasFailed());
    }

    public function testIsShorterEqualThan(): void
    {
        self::assertTrue(Validator::value('1')->isString()->isShorterEqualThan(0)->hasFailed());

        self::assertFalse(Validator::value('')->isString()->isShorterEqualThan(0)->hasFailed());
        self::assertFalse(Validator::value('1')->isString()->isShorterEqualThan(1)->hasFailed());
        self::assertFalse(Validator::value('12')->isString()->isShorterEqualThan(2)->hasFailed());
        self::assertFalse(Validator::value('1')->isString()->isShorterEqualThan(2)->hasFailed());
        self::assertFalse(Validator::value('12')->isString()->isShorterEqualThan(3)->hasFailed());
    }

    public function testIsLongerThan(): void
    {
        self::assertTrue(Validator::value('')->isString()->isLongerThan(0)->hasFailed());
        self::assertTrue(Validator::value('1')->isString()->isLongerThan(1)->hasFailed());
        self::assertTrue(Validator::value('1')->isString()->isLongerThan(1)->hasFailed());
        self::assertTrue(Validator::value('12')->isString()->isLongerThan(2)->hasFailed());
        self::assertTrue(Validator::value('12')->isString()->isLongerThan(3)->hasFailed());

        self::assertFalse(Validator::value('1')->isString()->isLongerThan(0)->hasFailed());
        self::assertFalse(Validator::value('12')->isString()->isLongerThan(1)->hasFailed());
        self::assertFalse(Validator::value('123')->isString()->isLongerThan(2)->hasFailed());
    }

    public function testIsLongerEqualThan(): void
    {
        self::assertTrue(Validator::value('')->isString()->isLongerEqualThan(1)->hasFailed());
        self::assertTrue(Validator::value('1')->isString()->isLongerEqualThan(2)->hasFailed());

        self::assertFalse(Validator::value('')->isString()->isLongerEqualThan(0)->hasFailed());
        self::assertFalse(Validator::value('1')->isString()->isLongerEqualThan(0)->hasFailed());
        self::assertFalse(Validator::value('1')->isString()->isLongerEqualThan(1)->hasFailed());
        self::assertFalse(Validator::value('12')->isString()->isLongerEqualThan(1)->hasFailed());
        self::assertFalse(Validator::value('12')->isString()->isLongerEqualThan(2)->hasFailed());
        self::assertFalse(Validator::value('123')->isString()->isLongerEqualThan(2)->hasFailed());
        self::assertFalse(Validator::value('123')->isString()->isLongerEqualThan(3)->hasFailed());
    }
}
