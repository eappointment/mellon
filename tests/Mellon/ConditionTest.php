<?php

declare(strict_types=1);

namespace BO\Mellon\Tests;

use BO\Mellon\Condition;
use BO\Mellon\Validator;
use PHPUnit\Framework\TestCase;

class ConditionTest extends TestCase
{
    public function testCondition(): void
    {
        $good = new Validator([
            'id' => 123,
            'email' => 'developer@example.com',
            'birthday' => '1999-09-09',
        ]);
        $bad = new Validator([
            'id' => 456,
            'email' => 'young-guy@example.com',
            'birthday' => (new \DateTime())->format('Y-m-d'),
        ]);

        $hasFailed = new Condition(
            $good->getParameter('id')->isNumber()->isRequired(),
            $good->getParameter('email')->isMail(),
            $good->getParameter('birthday')->isDate('Y-m-d')->isOldEnough()
        );

        self::assertTrue(!$hasFailed());

        $hasFailed = new Condition(
            $bad->getParameter('id')->isNumber()->isRequired(),
            $bad->getParameter('email')->isMail(),
            $bad->getParameter('birthday')->isDate('Y-m-d')->isOldEnough()
        );

        self::assertFalse(!$hasFailed());
    }
}
