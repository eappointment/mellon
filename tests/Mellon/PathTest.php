<?php
/**
 * @package Mellon
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

namespace BO\Mellon\Tests;

use BO\Mellon\Validator;
use PHPUnit\Framework\TestCase;

class PathTest extends TestCase
{

    public function testPath(): void
    {
        $value = "xyz/abc def.ghi";
        $invalidValue = Validator::value($value);
        $validValue = $invalidValue->isPath();
        $this->assertEquals($value, "$validValue");

        $value = "../abc";
        $this->assertTrue(
            Validator::value($value)->isPath()->hasFailed(),
            "URL '$value' should not validate"
        );

        $value = "abc && rm .htaccess";
        $this->assertTrue(
            Validator::value($value)->isPath()->hasFailed(),
            "URL '$value' should not validate"
        );

        self::assertFalse(Validator::value('')->isPath()->hasFailed());
        self::assertTrue(Validator::value(null)->isPath()->hasFailed());
    }
}
