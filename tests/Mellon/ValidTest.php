<?php

declare(strict_types=1);

namespace BO\Mellon\Tests;

use BO\Mellon\Validator;
use PHPUnit\Framework\TestCase;

class ValidTest extends TestCase
{
    protected $sut;

    public function setUp(): void
    {
        $post = [
            'string'  => 'good',
            'case'    => 'worst',
            'number'  => '0',
            'zero'    => 0,
            'one'     => 1,
            'strTwo'  => '2',
            'empty'   => '',
            'empty2'  => [],
        ];

        $this->sut = new Validator($post);
    }

    public function testIsDeclared(): void
    {
        self::assertFalse($this->sut->getParameter('string')->isDeclared()->hasFailed());
        self::assertFalse($this->sut->getParameter('empty')->isDeclared()->hasFailed());
        self::assertFalse($this->sut->getParameter('empty2')->isDeclared()->hasFailed());
        self::assertTrue($this->sut->getParameter('nothing')->isDeclared()->hasFailed());
    }

    public function testIsDevoidOf(): void
    {
        self::assertFalse($this->sut->getParameter('string')->isDevoidOf(['bad', 'worse', 'worst'])->hasFailed());
        self::assertTrue($this->sut->getParameter('case')->isDevoidOf(['bad', 'worse', 'worst'])->hasFailed());
    }

    public function testIsEqualTo(): void
    {
        self::assertFalse($this->sut->getParameter('nothing')->isEqualTo(null)->hasFailed());
        self::assertFalse($this->sut->getParameter('nothing')->isEqualTo(0)->hasFailed());
        self::assertFalse($this->sut->getParameter('nothing')->isEqualTo('')->hasFailed());
        self::assertFalse($this->sut->getParameter('nothing')->isEqualTo(false)->hasFailed());

        self::assertFalse($this->sut->getParameter('zero')->isEqualTo(0)->hasFailed());
        self::assertFalse($this->sut->getParameter('number')->isEqualTo(0)->hasFailed());
        if (PHP_VERSION < 8) {
            self::assertFalse($this->sut->getParameter('empty')->isEqualTo(0)->hasFailed()); // php7.3 false
        } else {
            self::assertTrue($this->sut->getParameter('empty')->isEqualTo(0)->hasFailed()); // php8.0 true
        }
        self::assertFalse($this->sut->getParameter('zero')->isEqualTo('0')->hasFailed());
        self::assertFalse($this->sut->getParameter('number')->isEqualTo('0')->hasFailed());
        self::assertFalse($this->sut->getParameter('one')->isEqualTo(true)->hasFailed());
        self::assertFalse($this->sut->getParameter('zero')->isEqualTo(false)->hasFailed());
        self::assertFalse($this->sut->getParameter('empty')->isEqualTo(false)->hasFailed());

        self::assertTrue($this->sut->getParameter('empty')->isEqualTo('0')->hasFailed());
        self::assertTrue($this->sut->getParameter('string')->isEqualTo('failed')->hasFailed());
    }

    public function testIsNotEqualTo(): void
    {
        self::assertTrue($this->sut->getParameter('zero')->isNotEqualTo(0)->hasFailed());
        self::assertTrue($this->sut->getParameter('number')->isNotEqualTo(0)->hasFailed());
        if (PHP_VERSION < 8) {
            self::assertTrue($this->sut->getParameter('empty')->isNotEqualTo(0)->hasFailed()); // php7.3 false
        } else {
            self::assertFalse($this->sut->getParameter('empty')->isNotEqualTo(0)->hasFailed()); // php8.0 true
        }
        self::assertTrue($this->sut->getParameter('zero')->isNotEqualTo('0')->hasFailed());
        self::assertTrue($this->sut->getParameter('number')->isNotEqualTo('0')->hasFailed());
        self::assertTrue($this->sut->getParameter('one')->isNotEqualTo(true)->hasFailed());
        self::assertTrue($this->sut->getParameter('zero')->isNotEqualTo(false)->hasFailed());
        self::assertTrue($this->sut->getParameter('empty')->isNotEqualTo(false)->hasFailed());

        self::assertFalse($this->sut->getParameter('empty')->isNotEqualTo('0')->hasFailed());
        self::assertFalse($this->sut->getParameter('string')->isNotEqualTo('failed')->hasFailed());
    }

    public function testIsOneOf(): void
    {
        self::assertFalse($this->sut->getParameter('string')->isOneOf(['best', 'better', 'good'])->hasFailed());
        self::assertTrue($this->sut->getParameter('case')->isOneOf(['best', 'better', 'good'])->hasFailed());
    }

    public function testIsRequired(): void
    {
        self::assertTrue($this->sut->getParameter('empty')->isRequired()->hasFailed());
        self::assertTrue($this->sut->getParameter('number')->isRequired()->hasFailed());
        self::assertTrue($this->sut->getParameter('empty2')->isRequired()->hasFailed());
        self::assertFalse($this->sut->getParameter('string')->isRequired()->hasFailed());
        self::assertFalse($this->sut->getParameter('one')->isRequired()->hasFailed());
    }
}
