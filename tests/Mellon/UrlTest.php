<?php
/**
 * @package Mellon
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

namespace BO\Mellon\Tests;

use BO\Mellon\Validator;
use PHPUnit\Framework\TestCase;

class UrlTest extends TestCase
{

    public function testUrl(): void
    {
        $value = "http://www.domaid.tld/sub/path/file.html";
        $this->assertFalse(
            Validator::value($value)->isUrl()->hasFailed(),
            "URL '$value' should validate"
        );
        $value = "abc";
        $this->assertTrue(
            Validator::value($value)->isUrl()->hasFailed(),
            "URL '$value' should not validate"
        );
    }
}
