<?php
/**
 * @package Mellon
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

namespace BO\Mellon\Tests;

use ArrayObject;
use stdClass;
use BO\Mellon\Validator;
use PHPUnit\Framework\TestCase;

class ArrayTest extends TestCase
{

    public function testArray()
    {
        $value = [1, 2, 3];
        $this->assertFalse(
            Validator::value($value)->isArray()->hasFailed(),
            "Native PHP array should validate"
        );
        $value = new ArrayObject($value);
        $this->assertFalse(
            Validator::value($value)->isArray()->hasFailed(),
            "ArrayObject should validate"
        );
        $value = new stdClass();
        $value->a = 1;
        $value->b = 2;
        $this->assertTrue(
            Validator::value($value)->isArray()->hasFailed(),
            "stdClass should not validate"
        );

        self::assertSame(null, Validator::value('[]')->isArray()->getValue());
        self::assertSame([], Validator::value([])->isArray()->getValue());
        self::assertFalse(Validator::value(['test' => null])->isArray()->hasFailed());
        self::assertTrue(Validator::value([])->isArray()->isRequired()->hasFailed());
        self::assertSame(
            ['a' => 'b'],
            Validator::value([])->isArray()->isRequired()->setDefault(['a' => 'b'])->getValue()
        );
    }

    public function testHasOnlyNumbers()
    {
        self::assertTrue(Validator::value(['test' => null])->isArray()->hasOnlyNumbers()->hasFailed());

        $value = [
            0,
            '2',
            '3.4',
            ['test' => 1],
        ];
        self::assertFalse(Validator::value($value)->isArray()->hasOnlyNumbers()->hasFailed());
        $value[3]['test'] = null;

        self::assertTrue(Validator::value($value)->isArray()->hasOnlyNumbers()->hasFailed());
    }

    public function testHasNoEmptyEntries()
    {
        self::assertFalse(Validator::value([])->isArray()->hasNoEmptyEntries()->hasFailed());
        self::assertFalse(Validator::value(['test' => 0])->isArray()->hasNoEmptyEntries()->hasFailed());
        self::assertFalse(Validator::value(['test' => 0.0])->isArray()->hasNoEmptyEntries()->hasFailed());
        self::assertFalse(Validator::value(['test' => false])->isArray()->hasNoEmptyEntries()->hasFailed());

        self::assertTrue(Validator::value('[]')->isArray()->hasNoEmptyEntries()->hasFailed());
        self::assertTrue(Validator::value(['test' => null])->isArray()->hasNoEmptyEntries()->hasFailed());
        self::assertTrue(Validator::value(['test' => []])->isArray()->hasNoEmptyEntries()->hasFailed());
        self::assertTrue(Validator::value(['test' => ''])->isArray()->hasNoEmptyEntries()->hasFailed());

        self::assertFalse(Validator::value([
            'test' => false,
            'sub'  => [
                'key' => 'value',
                1  => 1
            ]
        ])->isArray()->hasNoEmptyEntries()->hasFailed());

        self::assertTrue(Validator::value([
            'test' => 'fails',
            'sub'  => [
                'key' => 'value',
                'empty' => null
            ]
        ])->isArray()->hasNoEmptyEntries()->hasFailed());
    }

    public function testHasNestingLevel(): void
    {
        $anArray = ['floor1' => ['room101', 'room102'], 'floor2' => ['room201']];
        self::assertFalse(Validator::value($anArray)->isArray()->hasNestingLevel(2)->hasFailed());
        self::assertFalse(
            Validator::value(['floor1' => [], 'floor2' => []])->isArray()->hasNestingLevel(2)->hasFailed()
        );

        self::assertTrue(Validator::value($anArray)->isArray()->hasNestingLevel(3)->hasFailed());
        self::assertTrue(
            Validator::value(['floor1' => ['room101'], 'floor2' => null])->isArray()->hasNestingLevel(2)->hasFailed()
        );
    }

    public function testIsDevoidOf(): void
    {
        self::assertTrue(Validator::value(['good', 'suspicious'])->isArray()->isDevoidOf(['suspicious'])->hasFailed());
        self::assertTrue(Validator::value('good,suspicious')->isArray()->isDevoidOf(['suspicious'])->hasFailed());
        self::assertFalse(Validator::value(['good', 'well'])->isArray()->isDevoidOf(['suspicious'])->hasFailed());
        self::assertTrue(
            Validator::value(['warning', 'critical'])->isArray()->isDevoidOf(['error', 'critical'])->hasFailed()
        );
    }
}
