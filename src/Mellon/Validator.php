<?php
/**
 * @package Mellon
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

namespace BO\Mellon;

use InvalidArgumentException;
use Psr\Http\Message\UploadedFileInterface;

/**
  * Validate external parameters
  *
  */
class Validator
{
    /**
      * Parameters to validate
      *
      * @var array $parameters
      */
    protected $parameters = array();

    /**
     * Content of STDIN
     *
     * @var string $input
     */
    protected $input = null;

    /**
     * @var array|UploadedFileInterface[]
     */
    protected $files = [];

    /**
      * Singleton instance
      *
      * @var self $instance
      */
    protected static $instance = null;

    /**
     * Always initialize using an array of parameters
     *
     * @throws Exception
     */
    public function __construct($parameters, $input = null, array $files = [])
    {
        $this->setParameters($parameters);
        $this->setInput($input);
        $this->setFiles($files);
    }

    /**
     * @param $parameters
     * @return self
     * @throws Exception
     */
    public function setParameters($parameters): Validator
    {
        if (!is_array($parameters)) {
            throw new Exception("Array argument required for parameters");
        }
        $this->parameters = $parameters;
        return $this;
    }

    /**
     * @param mixed $input
     * @return self
     */
    public function setInput($input): Validator
    {
        $this->input = $input;
        return $this;
    }

    public function setFiles(array $files): Validator
    {
        $this->files = $files;

        return $this;
    }

    /**
     * @SuppressWarnings(Superglobals)
     * @return self
     * @throws Exception
     */
    public static function getInstance(): Validator
    {
        if (null === self::$instance) {
            self::$instance = new self($_REQUEST);
        }
        return self::$instance;
    }

    /**
     * @return void
     */
    public static function resetInstance(): void
    {
        self::$instance = null;
    }

    /**
     * @return self
     */
    public function makeInstance(): Validator
    {
        self::$instance = $this;
        return $this;
    }

    /**
     * @param string $name
     * @return bool
     */
    public function hasParameter(string $name): bool
    {
        return array_key_exists($name, $this->parameters);
    }

    /**
     * @param string $name
     * @return Unvalidated
     */
    public function getParameter(string $name): Unvalidated
    {
        if ($this->hasParameter($name)) {
            return new Unvalidated($this->parameters[$name], $name);
        }
        return new Unvalidated(null, $name);
    }

    /**
     * Validate a key from the given parameters
     *
     * @param string $name of the key
     *
     * @return Unvalidated
     * @throws Exception
     */
    public static function param(string $name): Unvalidated
    {
        $validator = self::getInstance();
        return $validator->getParameter($name);
    }

    /**
     * Validate a mixed value
     *
     * @param mixed $mixed
     * @param string $name an optional name to identify the value
     *
     * @return Unvalidated
     */
    public static function value($mixed, string $name = ''): Unvalidated
    {
        return new Unvalidated($mixed, $name);
    }

    /**
     * Validate content of STDIN, usually the body of an HTTP request
     *
     * @param string $name an optional name to identify the value
     *
     * @return Unvalidated
     * @throws Exception
     */
    public static function input(string $name = ''): Unvalidated
    {
        $validator = self::getInstance();
        return $validator->getInput($name);
    }

    /**
     * @param string $name the name to identify the file
     *
     * @return ValidFile
     * @throws InvalidArgumentException
     */
    public function getFile(string $name): ValidFile
    {
        return new ValidFile($this->files[$name] ?? [], $name);
    }

    /**
     * @param string $name an optional name to identify the value
     *
     * @return Unvalidated
     */
    public function getInput(string $name = ''): Unvalidated
    {
        if (null === $this->input) {
            $this->input = file_get_contents('php://input');
        }
        return self::value($this->input, $name);
    }

    /**
     * @param array $validatorList
     * @return Collection
     * @throws Exception
     */
    public static function collection(array $validatorList): Collection
    {
        return new Collection($validatorList);
    }
}
