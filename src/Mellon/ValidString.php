<?php
/**
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

namespace BO\Mellon;

/**
  * Validation of Strings
  * This validation is opinionated: It sanitizes the output from special chars for HTML
  */
class ValidString extends Valid
{

    /**
     * Allow strings smaller than 64kb and do htmlspecialchars()
     *
     * @param string $message error message in case of failure
     * @param bool $sanitize
     *
     * @return self
     */
    public function isString(string $message = 'no valid string', bool $sanitize = true): ValidString
    {
        if (!$sanitize && !is_string($this->value)) {
            return $this->setFailure($message);
        } elseif (!is_scalar($this->value)) { // values that are not a string will be stringified (except null)
            return $this->setFailure($message);
        }

        $this->isShorterThan(65536, $message);
        if ($sanitize) {
            return $this->validate($message, FILTER_SANITIZE_SPECIAL_CHARS);
        }

        return $this;
    }

    /**
     * Allow only strings which do not match a given regular expression
     *
     * @param string $regex Regular expression including delimiter and modifier
     * @param string $message error message in case of failure
     *
     * @return self
     */
    public function isFreeOf(string $regex, string $message = 'value contains undesired content'): ValidString
    {
        if ($this->hasFailed()) {
            return $this;
        }
        $this->validated = true;

        if (preg_match($regex, (string) $this->value)) {
            $this->setFailure($message);
        }
        return $this;
    }

    /**
     * Allow only strings which match a given regular expression
     *
     * @param string $regex Regular expression including delimiter and modifier
     * @param string $message error message in case of failure
     *
     * @return self
     */
    public function isMatchOf(string $regex, string $message = 'not a valid matching value'): ValidString
    {
        if ($this->hasFailed()) {
            return $this;
        }
        $this->isDeclared($message);
        return $this->validate($message, FILTER_VALIDATE_REGEXP, array(
            'options' => array(
                'regexp' => $regex,
            ),
        ));
    }

    /**
     * Allow only strings with a length bigger than the given value
     *
     * @deprecated (does not work as the description says)
     * @param int $size value to compare length of the string
     * @param string $message error message in case of failure
     *
     * @return self
     */
    public function isBiggerThan(int $size, string $message = 'too small'): ValidString
    {
        if ($this->hasFailed()) {
            return $this;
        }
        $this->validated = true;
        if (strlen((string) $this->value) < $size) {
            $this->setFailure($message);
        }
        return $this;
    }

    /**
     * Allow only strings with a length higher than the given value
     *
     * @param int $length value to compare length of the string
     * @param string $message error message in case of failure
     *
     * @return self
     */
    public function isLongerThan(int $length, string $message = 'too short'): ValidString
    {
        if ($this->hasFailed()) {
            return $this;
        }
        $this->validated = true;
        if (!(strlen((string) $this->value) > $length)) {
            $this->setFailure($message);
        }

        return $this;
    }

    /**
     * Allow only strings with a length higher or equal than the given value
     *
     * @param int $length value to compare length of the string
     * @param string $message error message in case of failure
     *
     * @return self
     */
    public function isLongerEqualThan(int $length, string $message = 'too short'): ValidString
    {
        if ($this->hasFailed()) {
            return $this;
        }
        $this->validated = true;
        if (!(strlen((string) $this->value) >= $length)) {
            $this->setFailure($message);
        }

        return $this;
    }

    /**
     * Allow only strings with a length smaller than the given value
     *
     * @deprecated (does not work as the description says)
     * @param int $size value to compare length of the string
     * @param string $message error message in case of failure
     *
     * @return self
     */
    public function isSmallerThan(int $size, string $message = 'too big'): ValidString
    {
        if ($this->hasFailed()) {
            return $this;
        }
        $this->validated = true;
        if (strlen((string) $this->value) > $size) {
            $this->setFailure($message);
        }
        return $this;
    }

    /**
     * Allow only strings with a length lower than the given value
     *
     * @param int $length value to compare length of the string
     * @param string $message error message in case of failure
     *
     * @return self
     */
    public function isShorterThan(int $length, string $message = 'too long'): ValidString
    {
        if ($this->hasFailed()) {
            return $this;
        }
        $this->validated = true;
        if (!(strlen((string) $this->value) < $length)) {
            $this->setFailure($message);
        }

        return $this;
    }

    /**
     * Allow only strings with a length lower or equal than the given value
     *
     * @param int $length value to compare length of the string
     * @param string $message error message in case of failure
     *
     * @return self
     */
    public function isShorterEqualThan(int $length, string $message = 'too long'): ValidString
    {
        if ($this->hasFailed()) {
            return $this;
        }
        $this->validated = true;
        if (!(strlen((string) $this->value) <= $length)) {
            $this->setFailure($message);
        }

        return $this;
    }
}
