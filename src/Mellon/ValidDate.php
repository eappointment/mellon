<?php
/**
 * @package Mellon
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

namespace BO\Mellon;

use DateTime;

/**
  * Validation if the value has an expected date format
  */
class ValidDate extends ValidDateTime
{
    /**
     * Allow only valid date format
     *
     * @param string $format expected format of the Date like m.d.Y
     * @param string $message error message in case of failure
     *
     * @return self
     */
    public function isDate(string $format = 'U', string $message = 'no valid date'): ValidDate
    {
        $this->validated = true;
        if (!$this->value) {
            return $this->setFailure($message);
        }

        $selectedDate = DateTime::createFromFormat($format, $this->value);
        //$selectedDate->setTimezone(new \DateTimeZone(\App::TIMEZONE));
        $isDate = $selectedDate !== false && $selectedDate->getTimestamp() > 0;
        if (false === $isDate) {
            $this->setFailure($message);
        } else {
            $this->dateTime = $selectedDate;
        }

        return $this;
    }
}
