<?php
/**
 * @package Mellon
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

namespace BO\Mellon;

use BO\Mellon\Failure\Message;
use BO\Mellon\Failure\MessageList;

/**
 * Parameter validation
 *
 * @SuppressWarnings(TooManyMethods)
 */
class Valid extends Parameter
{

    /**
      * validation errors
      *
      * @var MessageList|null $messages
      */
    private $messages = null;

    /**
      * TRUE if value is validated, at least once
      *
      * @var bool  $validated
      */
    protected $validated = false;

    /**
      * TRUE if validation failed
      *
      * @var bool  $failed
      */
    protected $failed = false;

    /**
      * default value to return when the validation fails
      *
      * @var string $default
      */
    protected $default = null;

    /**
     * validate a value using PHP builtin function filter_var()
     *
     * @param string $message error message in case of failure
     * @param int $filter see documentation for filter_var()
     * @param array|int $options see documentation for filter_var()
     *
     * @return self
     */
    protected function validate(string $message, int $filter, $options = []): Valid
    {
        if (null !== $this->value) {
            $this->validated = true;
            $filtered = filter_var($this->value, $filter, $options);
            if (($filtered === false && $filter !== FILTER_VALIDATE_BOOLEAN) || $filtered === null) {
                $this->setFailure(new Message($message));
            } else {
                $this->value = $filtered;
            }
        }
        return $this;
    }

    protected function setFailureMessage($message): Valid
    {
        if (null === $this->messages) {
            $this->messages = new Failure\MessageList();
        }
        if ($message instanceof Failure\MessageList) {
            foreach ($message as $item) {
                $this->messages[] = $item;
            }
        } elseif ($message instanceof Failure\Message) {
            $this->messages[] = $message;
        } else {
            $this->messages[] = new Failure\Message($message);
        }
        return $this;
    }

    /**
     * Set state to failed and add a message
     *
     * @param string|Message|MessageList $message error message in case of failure
     *
     * @return self
     */
    public function setFailure($message): Valid
    {
        $this->failed = true;
        return $this->setFailureMessage($message);
    }

    /**
     * Do not allow NULL as value
     *
     * @param string $message error message in case of failure
     *
     * @return self
     */
    public function isDeclared(string $message = 'value is not declared'): Valid
    {
        if (null === $this->value) {
            $this->setFailure($message);
        }
        return $this;
    }

    /**
     * Require a value
     * ATTENTION: Does not allow "0" (zero) as value
     *
     * @param string $message error message in case of failure
     *
     * @return self
     */
    public function isRequired(string $message = 'value is not declared'): Valid
    {
        if (!$this->value) {
            $this->setFailure($message);
        }
        return $this;
    }

    /**
     * Allow only values equal to the given value
     *
     * @param mixed $value value to compare
     * @param string $message error message in case of failure
     *
     * @return self
     */
    public function isEqualTo($value, string $message = 'not matching'): Valid
    {
        $this->validated = true;
        if ($this->value != $value) {
            $this->setFailure($message);
        }
        return $this;
    }

    /**
     * Allow only values not equal to the given value
     *
     * @param mixed $value value to compare
     * @param string $message error message in case of failure
     *
     * @return self
     */
    public function isNotEqualTo($value, string $message = 'not unequal'): Valid
    {
        $this->validated = true;
        if ($this->value == $value) {
            $this->setFailure($message);
        }
        return $this;
    }

    /**
     * allow only values that are in the list
     *
     * @param array $list value to compare
     * @param string $message error message in case of failure
     *
     * @return self
     */
    public function isOneOf(array $list, string $message = 'not matching'): Valid
    {
        $this->validated = true;
        $failed = true;
        foreach ($list as $value) {
            if ($this->value == $value) {
                $failed = false;
                break;
            }
        }

        if ($failed) {
            $this->setFailure($message);
        }

        return $this;
    }

    /**
     * allow only values that are not in the list
     *
     * @param array $list value to compare
     * @param string $message error message in case of failure
     *
     * @return self
     */
    public function isDevoidOf(array $list, string $message = 'not matching'): Valid
    {
        $this->validated = true;
        foreach ($list as $value) {
            if ($this->value == $value) {
                $this->setFailure($message);
                return $this;
            }
        }
        return $this;
    }

    /**
     * Get the validated value or the default value
     *
     * @return mixed
     */
    public function getValue()
    {
        if ($this->hasFailed() || !$this->validated) {
            return $this->default;
        }
        return $this->value;
    }

    /**
     * Get the validated value or the default value as string
     *
     * @return string
     */
    public function __toString()
    {
        $value = $this->getValue();
        if (null === $value) {
            $value = '';
        }
        return (string)$value;
    }

    /**
     * Set a default value to return if a string does not validate
     *
     * @param mixed $value
     *
     * @return self
     */
    public function setDefault($value): Valid
    {
        $this->default = $value;
        return $this;
    }

    /**
     * True if validation has failed
     *
     * @return bool
     */
    public function hasFailed(): bool
    {
        return $this->failed;
    }


    /**
     * Throws exception if validation fails
     *
     * @throws Failure\Exception
     * @return Valid
     */
    public function assertValid(): Valid
    {
        if ($this->hasFailed()) {
            $exception = new Failure\Exception();
            $exception->setValidator($this);
            throw $exception;
        }
        return $this;
    }

    /**
     * Returns a list of error messages
     *
     * @return MessageList|Message[]|null
     */
    public function getMessages(): ?MessageList
    {
        return $this->messages;
    }

    /**
     * Returns a hash for usage in templates engines
     * Contains the following keys:
     *     failed - True if validation has failed
     *     messages - A list of error messages in case the validation has failed
     *     value - Value, might be the default value if validation has failed
     *
     * @param bool $unvalidated (optional) Return original value
     *
     * @return array
     */
    public function getStatus(bool $unvalidated = false): array
    {
        $status = array(
            'failed' => $this->failed,
            'value' => $this->getValue(),
            'messages' => (array) $this->getMessages(),
        );
        if ($unvalidated) {
            $status['_unvalidated'] = $this->getUnvalidated();
        }
        return $status;
    }

    public function getUnvalidated()
    {
        return $this->value;
    }

    /**
     * Throw an exception with a descriptive warning
     *
     * @throws \Exception
     */
    public function __call($name, $arguments)
    {
        if (0 === strpos($name, 'is')) {
            throw new Exception(
                "the validation $name() is not defined in class " . get_class($this) . ". Read the manual."
            );
        }
        throw new Exception("function $name is not defined in " . get_class($this));
    }
}
