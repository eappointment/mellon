<?php
/**
 * @package Mellon
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

namespace BO\Mellon;

/**
  * Validation of URLs
  *
  */
class ValidUrl extends ValidString
{
    /**
     * Allow only valid urls
     *
     * @param string $message error message in case of failure
     * @see Unvalidated::__call
     * @return self
     */
    public function isUrl(string $message = 'no valid url'): ValidUrl
    {
        $this->isDeclared($message);
        $this->isString($message);

        return $this->validate($message, FILTER_VALIDATE_URL);
    }
}
