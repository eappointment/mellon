<?php
/**
 * @package Mellon
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

namespace BO\Mellon;

use InvalidArgumentException;

/**
 * Parameter validation
 *
 * @method Valid isDeclared(string $message = 'value is not declared')
 * @uses Valid::isDeclared()
 * @method Valid isDevoidOf(array $list, string $message = 'not matching')
 * @uses Valid::isDevoidOf()
 * @method Valid isEqualTo(mixed $value, string $message = 'not matching')
 * @uses Valid::isEqualTo()
 * @method Valid isNotEqualTo(mixed $value, string $message = 'not matching')
 * @uses Valid::isNotEqualTo()
 * @method Valid isOneOf(array $list, string $message = 'not matching')
 * @uses Valid::isOneOf()
 * @method Valid isRequired(string $message = 'value is not declared')
 * @uses Valid::isRequired()
 * @method ValidArray isArray(string $message = 'no valid array')
 * @method ValidBool isBool(string $message = 'not a boolean value')
 * @method ValidDate isDate(string $format = 'U', string $message = 'no valid date')
 * @method ValidDateTime isDateTime(string $format = null, string $message = 'Please enter a valid date')
 * @method ValidJson isJson(string $message = null)
 * @method ValidMail isMail(string $message = 'no valid email')
 * @method ValidNumber isNumber(string $message = 'no valid number')
 * @method ValidPath isPath(string $message = 'no valid path')
 * @method ValidString isString(string $message = 'no valid string', bool $sanitize = true)
 * @method ValidUrl isUrl(string $message = 'no valid url')
 */
class Unvalidated extends Parameter
{
    /**
     * @var callable $setValid
     */
    protected $setValid;

    /**
     * Return a valid parameter
     * this function changes class to verify validation
     *
     * @return Valid
     * @throws Exception
     *
     * @uses ValidArray::isArray
     * @uses ValidBool::isBool
     * @uses ValidDate::isDate()
     * @uses ValidDateTime::isDateTime()
     * @uses ValidJson::isJson()
     * @uses ValidMail::isMail()
     * @uses ValidNumber::isNumber()
     * @uses ValidPath::isPath()
     * @uses ValidString::isString()
     * @uses ValidUrl::isUrl()
     *
     */
    public function __call($name, $arguments)
    {
        $valid = new Valid($this->value, $this->name);
        if (!method_exists($valid, $name)) {
            $valid = $this->findTypedValidator($name);
        }
        return call_user_func_array(array($valid, $name), $arguments);
    }

    /**
     * @throws Exception
     */
    public function getValue()
    {
        throw new Exception("Parameters should validate first.");
    }

    /**
     * replace a certain value before it is validated (NULL stands for a parameter that does not exist)
     * ->default value settable with a Validation-Collection
     *
     * @param scalar|null $checkedFor
     * @param scalar|null $replaceValue
     * @return Unvalidated
     * @throws InvalidArgumentException
     */
    public function replace($checkedFor, $replaceValue): Unvalidated
    {
        if ($this->value === $checkedFor) {
            $this->value = $replaceValue;
        }

        return $this;
    }

    /**
     * Set a callback to receive a reference on the validated object
     * The first parameter of this callback receives the validated object
     *
     * @param callable $setValid
     * @return Unvalidated
     */
    public function setCallback(callable $setValid): Unvalidated
    {
        $this->setValid = $setValid;
        return $this;
    }

    /**
     * Try to find a class for a validation function
     *
     * @param string $name like "isUrl" where "ValidUrl" is an existing class
     *
     * @return Valid
     * @throws Exception
     *
     */
    protected function findTypedValidator(string $name): Valid
    {
        $partList = preg_split('#([A-Z][a-z]+)#', $name, -1, PREG_SPLIT_DELIM_CAPTURE);
        if (isset($partList[1])) {
            $class = __NAMESPACE__ . '\\Valid' . $partList[1];
            if (class_exists($class)) {
                $newClass = new $class($this->value, $this->name);
                if ($this->setValid) {
                    $callback = $this->setValid;
                    $callback($newClass);
                }
                return $newClass;
            } else {
                throw new Exception("Validation class $class does not exists");
            }
        }
        throw new Exception("invalid validation function");
    }
}
