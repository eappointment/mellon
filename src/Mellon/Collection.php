<?php
/**
 * @package Mellon
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

namespace BO\Mellon;

use RecursiveArrayIterator;
use RecursiveIteratorIterator;

/**
  * validatorList for Validation tests
  */
class Collection
{
    /**
     * Hash of validators
     *
     * @var array $validatorList
     * @throws \Exception
     */
    protected $validatorList = array();

    public function __construct(array $validatorList)
    {
        $this->validatorList = $validatorList;
        $flat = $this->getFlatArray();
        foreach ($flat as $key => $valid) {
            if (!$valid instanceof Valid) {
                throw new Exception("No Valid value for $key");
            }
        }
    }

    /**
     * @return array
     */
    protected function getFlatArray(): array
    {
        $arrayIterator = new RecursiveArrayIterator(array($this->validatorList));
        $iterator = new RecursiveIteratorIterator($arrayIterator, RecursiveIteratorIterator::SELF_FIRST);

        return array_filter(iterator_to_array($iterator), function ($value) {
            return !is_array($value);
        });
    }

    /**
     * @return bool
     */
    public function hasFailed(): bool
    {
        $flat = $this->getFlatArray();
        foreach ($flat as $valid) {
            if ($valid->hasFailed()) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param array $sub
     * @param bool $getUnvalidated
     * @return array
     */
    public function getStatus(array $sub = null, bool $getUnvalidated = false): array
    {
        $messages = array();
        if (null === $sub) {
            $sub = $this->validatorList;
        }
        foreach ($sub as $key => $value) {
            if ($value instanceof Valid) {
                $messages[$key] = $value->getStatus($getUnvalidated);
            } else {
                $messages[$key] = $this->getStatus($value, $getUnvalidated);
            }
        }
        return $messages;
    }

    /**
     * @return array
     */
    public function getValues(): array
    {
        return $this->validatorList;
    }


    public function addValid(Valid ...$validList): void
    {
        foreach ($validList as $valid) {
            if (strlen($valid->getName()) > 0) {
                $this->validatorList[$valid->getName()] = $valid;
            } else {
                $this->validatorList[] = $valid;
            }
        }
    }

    public function getValid($parameterName): ?Valid
    {
        if (isset($this->validatorList[$parameterName])) {
            return $this->validatorList[$parameterName];
        }

        return null;
    }

    public function validatedAction(Valid $valid, callable $action): self
    {
        $this->addValid($valid);
        if (!$valid->hasFailed()) {
            $action($valid->getValue());
        }
        return $this;
    }
}
