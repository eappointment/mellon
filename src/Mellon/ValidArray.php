<?php
/**
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

namespace BO\Mellon;

use ArrayAccess;
use BO\Mellon\Failure\Message;
use BO\Mellon\Failure\MessageList;
use Countable;
use Traversable;

/**
  * Validation of Strings
  * This validation is opinionated: It sanitizes the output from special chars for HTML
  */
class ValidArray extends Valid
{

    /**
     * Allow native arrays anc class replacements
     *
     * @param string $message error message in case of failure
     *
     * @return $this
     */
    public function isArray(string $message = 'no valid array'): ValidArray
    {
        if (is_array($this->value)
            || (
                $this->value instanceof Traversable
                && $this->value instanceof Countable
                && $this->value instanceof ArrayAccess
            )
        ) {
            $this->validated = true;
            return $this;
        } else {
            $this->setFailure($message);
        }

        return $this;
    }

    /**
     * @SuppressWarnings(CyclomaticComplexity)
     */
    public function hasOnlyNumbers(string $message = 'The array contained a non-numeric value.'): ValidArray
    {
        $messages = new MessageList();
        $messages->append(new Message($message));

        if (!$this->hasFailed()) {
            foreach ($this->value as $key => $value) {
                if (!Validator::value($value)->isArray()->hasFailed() && count($value) > 0) {
                    $sub = Validator::value($value)->isArray()->hasOnlyNumbers();
                    if ($sub->hasFailed()) {
                        $this->failed = true;
                        foreach ($sub->getMessages() as $index => $msg) {
                            if ($index === 0) {
                                continue;
                            }
                            $messages->append(new Message("Failure in the array at index '$key': " . $msg->message));
                        }
                    }
                } elseif (!is_numeric($value)) {
                    $messages->append(new Message("The index '$key' contained a non-numeric value."));
                    $this->failed = true;
                }
            }
            if ($this->hasFailed()) {
                $this->setFailure($messages);
            }
        }

        return $this;
    }

    /**
     * fails on empty array entries, where empty means only values like empty string, empty array or null
     * @SuppressWarnings(CyclomaticComplexity)
     *
     * @param string $message
     * @return $this
     */
    public function hasNoEmptyEntries(string $message = 'The array has an empty entry.'): ValidArray
    {
        if ($this->hasFailed()) {
            return $this;
        }

        $messages = new MessageList([new Message($message)]);

        foreach ($this->value as $key => $value) {
            if ($value === [] || $value === '' || $value === null) {
                $messages->append(new Message("The index $key contained an empty value."));
                $this->failed = true;
                continue;
            }

            $sub = Validator::value($value)->isArray();
            if ($sub->hasFailed() || count($value) === 0) {
                continue;
            }
            if ($sub->hasNoEmptyEntries($message)->hasFailed()) {
                $this->failed = true;
                foreach ($sub->getMessages() as $index => $msg) {
                    if ($index === 0) {
                        continue;
                    }
                    $messages->append(new Message('Failure in the array at index $key: ' . $msg->message));
                }
            }
        }

        $this->hasFailed() && $this->setFailure($messages);

        return $this;
    }

    /**
     * check whether the value is a consistently nested array of arrays until a certain level
     *
     * @return void
     */
    public function hasNestingLevel(int $level, string $message = 'The array is not nested as expected.'): ValidArray
    {
        if (!$this->hasFailed() && $level > 1) {
            foreach ($this->value as $sub) {
                $subValidation = Validator::value($sub)->isArray()->hasNestingLevel($level - 1);
                if ($subValidation->hasFailed()) {
                    $this->setFailure($message);
                }
            }
        }

        return $this;
    }

    /**
     * allow only array which values are not in the list
     *
     * @param array $list value to compare
     * @param string $message error message in case of failure
     *
     * @return self
     */
    public function isDevoidOf(array $list, string $message = 'not matching'): Valid
    {
        if ($this->hasFailed()) {
            return $this;
        }

        foreach ($list as $value) {
            foreach ($this->value as $evaluated) {
                if ($evaluated == $value) {
                    $this->setFailure($message);
                    return $this;
                }
            }
        }
        return $this;
    }
}
