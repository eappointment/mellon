<?php
/**
 * @package Mellon
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

namespace BO\Mellon;

/**
  * Parameter validation
  */
abstract class Parameter
{

    /**
      * value of parameter
      *
      * @var mixed $value
      */
    protected $value = '';

    /**
      * name of parameter
      *
      * @var string $name
      */
    protected $name = '';

    /**
     * @param mixed $value
     * @param string $name
     */
    public function __construct($value, string $name = '')
    {
        $this->setValue($value);
        $this->setName($name);
    }

    /**
     * @param mixed $value
     * @return self
     */
    protected function setValue($value): Parameter
    {
        $this->value = $value;
        return $this;
    }

    /**
     * @param string $name
     * @return self
     */
    public function setName(string $name): Parameter
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }
}
