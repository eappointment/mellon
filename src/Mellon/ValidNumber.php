<?php
/**
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

namespace BO\Mellon;

/**
  * Validation of values containing an integer
  */
class ValidNumber extends Valid
{

    /**
     * Allow only integer numbers
     *
     * @param string $message error message in case of failure
     *
     * @return self
     */
    public function isNumber(string $message = 'no valid number'): ValidNumber
    {
        if (!is_scalar($this->value) || is_bool($this->value)) {
            return $this->setFailure($message);
        }

        return $this->validate($message, FILTER_VALIDATE_INT);
    }

    /**
     * Allow only numbers greater than the given value
     *
     * @param int $number value to compare
     * @param string $message error message in case of failure
     *
     * @return self
     */
    public function isGreaterThan(int $number, string $message = 'too small'): ValidNumber
    {
        $this->validated = true;
        if ($this->value <= $number) {
            $this->setFailure($message);
        }
        return $this;
    }

    /**
     * Allow only numbers greater or equal than the given value
     *
     * @param int $number value to compare
     * @param string $message error message in case of failure
     *
     * @return self
     */
    public function isGreaterEqualThan(int $number, string $message = 'too small'): ValidNumber
    {
        $this->validated = true;
        if ($this->value < $number) {
            $this->setFailure($message);
        }
        return $this;
    }

    /**
     * Allow only numbers lower or equal than the given value
     *
     * @param int $number value to compare
     * @param string $message error message in case of failure
     *
     * @return self
     */
    public function isLowerEqualThan(int $number, string $message = 'too small'): ValidNumber
    {
        $this->validated = true;
        if ($this->value > $number) {
            $this->setFailure($message);
        }
        return $this;
    }

    /**
     * Allow only numbers lower than the given value
     *
     * @param int $number value to compare
     * @param string $message error message in case of failure
     *
     * @return self
     */
    public function isLowerThan(int $number, string $message = 'too small'): ValidNumber
    {
        $this->validated = true;
        if ($this->value >= $number) {
            $this->setFailure($message);
        }
        return $this;
    }
}
