<?php
/**
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

namespace BO\Mellon;

/**
  * Validation if the value is equal to a boolean value or a common expression of it
  */
class ValidBool extends Valid
{

    /**
     * Allow only boolean values like
     * Allowed values are:
     *   true
     *   false
     *   yes
     *   no
     *   on
     *   off
     *   1
     *   0
     *   ''
     *
     * @param string $message error message in case of failure
     *
     * @return self
     */
    public function isBool(string $message = 'not a boolean value'): ValidBool
    {
        if ($this->value === null) {
            return $this->setFailure($message);
        }
        return $this->validate($message, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
    }
}
