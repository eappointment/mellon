<?php
/**
 * @package Mellon
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

namespace BO\Mellon;

/**
  * Validation of mail addresses
  */
class ValidMail extends ValidString
{
    /**
     * For unit tests, it might be necessary to disable DNS checks globally
     * Use \BO\Mellon\ValidMail::$disableDnsChecks = true;
     */
    public static $disableDnsChecks = false;

    /**
     * Allow only valid mails
     *
     * @param string $message error message in case of failure
     *
     * @return self
     */
    public function isMail(string $message = 'no valid email'): ValidMail
    {
        $this->isString($message);

        if (!$this->value) {
            return $this->setFailure($message);
        }

        $this->validate($message, FILTER_SANITIZE_EMAIL);
        return $this->validate($message, FILTER_VALIDATE_EMAIL);
    }

    /**
     * Not every DNS server refreshes an outdated entry on an ANY requests
     * So we have to check common types before checking an ANY type
     */
    protected function checkDnsAny($domain): bool
    {
        checkdnsrr($domain, 'A'); // refresh on outdated TTL
        checkdnsrr($domain, 'AAAA');
        checkdnsrr($domain, 'MX');
        return checkdnsrr($domain, 'ANY');
    }

    public function hasDNS($message = 'no valid DNS entry found'): ValidMail
    {
        $this->validated = true;
        if (is_string($this->value) && $this->value && !$this::$disableDnsChecks) {
            $domain = substr($this->value, strpos($this->value, '@') + 1);
            $hasDNS = ($domain) ? $this->checkDnsAny($domain) : false;
            if (false === $hasDNS) {
                $this->setFailure($message);
            }
        }
        return $this;
    }

    public function hasMX($message = 'no valid DNS entry of type MX found'): ValidMail
    {
        $this->validated = true;
        if ($this->value && is_string($this->value)) {
            $domain = substr($this->value, strpos($this->value, '@') + 1);
            $hasMX = checkdnsrr($domain, 'MX');
            if (false === $hasMX) {
                $this->setFailure($message);
            }
        }
        return $this;
    }
}
