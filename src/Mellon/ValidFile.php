<?php
/**
 * @package Mellon
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

namespace BO\Mellon;

use Psr\Http\Message\UploadedFileInterface;

/**
  * Validation of uploaded files
  *
  * @SuppressWarnings(Superglobals)
  */
class ValidFile extends Valid
{
    /**
     * @param array|UploadedFileInterface|UploadedFileInterface[] $value value of the uploaded file or else
     * @param string $name
     */
    public function __construct($value, string $name = '')
    {
        parent::__construct($value, $name);

        $this->isSingleFile(); // this makes the instance validated
    }

    /**
     * @see /etc/nginx/mime.types (or find the ones for your server app)
     */
    protected $acceptableTypeGroups = [
        'image' => [
            'bmp' => 'image/bmp',
            'bmp-ms' => 'image/x-ms-bmp',
            'jpeg' => 'image/jpeg',
            'jpg' => 'image/jpg',
            'gif' => 'image/gif',
            'png' => 'image/png',
            'svg' => 'image/svg+xml',
        ],
        'document' => [
            'md' => 'text/markdown',
            'pdf' => 'application/pdf',
            'rtf' => 'application/rtf',
            'html' => 'text/html',
            'txt' => 'text/plain',
            'doc' => 'application/msword',
            'docx' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            'xls' => 'application/vnd.ms-excel',
            'xlsx' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            'ppt' => 'application/vnd.ms-powerpoint',
            'pptx' => 'application/vnd.openxmlformats-officedocument.presentationml.slideshow',
            'ps' => 'application/postscript',
            'xhtml' => 'application/xhtml+xml',
            'xml' => 'text/xml',
            'xmla' => 'application/xml',
        ],
    ];

    /**
     * Allow only file references when they refer to a single file
     *
     * @param string $message error message in case of failure
     *
     * @return self
     */
    public function isSingleFile(string $message = 'No file found or the name referred to multiple files.'): ValidFile
    {
        $this->validated = true;
        if (empty($this->value)) {
            $this->setFailure($message);
        }
        if ($this->value instanceof UploadedFileInterface) {
            return $this->hasNoError();
        }

        if (is_array($this->value)) { // value can be a sub-entry from $_FILES or an array of UploadedFileInterface
            foreach ($this->value as $key => $value) {
                if ($key === 'tmp_name') {
                    if (empty($value)) {
                        return $this->setFailure($message);
                    }
                    break;
                }
                if (is_array($value)) {
                    return $this->setFailure($message);
                }
            }

            return $this->hasNoError();
        }

        return $this->setFailure($message);
    }

    public function hasNoError(string $message = 'An error occurred with the file upload.'): ValidFile
    {
        if ($this->value instanceof UploadedFileInterface) {
            $error = $this->value->getError();
        } else {
            $error = $this->value['error'] ?? UPLOAD_ERR_NO_FILE;
        }

        if ($error !== UPLOAD_ERR_OK) {
            $this->setFailure($message);
        }

        return $this;
    }

    public function isOfType(string $typeGroup = 'image', string $message = 'Invalid file type.'): ValidFile
    {
        if (!$this->hasFailed()) {
            if ($this->value instanceof UploadedFileInterface) {
                $type = $this->value->getClientMediaType();
            } else {
                $type = $this->value['type'] ?? '';
            }
            if (!isset($this->acceptableTypeGroups[$typeGroup])
                || !in_array($type, $this->acceptableTypeGroups[$typeGroup])
            ) {
                $this->setFailure($message);
            }
        }

        return $this;
    }

    /**
     * @param int $maxKBSize size in kByte
     * @param string $message
     * @return $this
     */
    public function hasMaxSize(int $maxKBSize = 50, string $message = 'File size not valid.'): ValidFile
    {
        $maxByteSize = $maxKBSize * 1024;

        if (!$this->hasFailed()) {
            if ($this->value instanceof UploadedFileInterface) {
                $size = $this->value->getSize();
            } else {
                $size = $this->value['size'] ?? 0;
            }

            if ($size > $maxByteSize) {
                $this->setFailure($message);
            }
        }

        return $this;
    }

    /**
     * @return array|UploadedFileInterface|null (if it is an array: ['tmp_name' => '...', 'error' => X, ...])
     */
    public function getValue()
    {
        return parent::getValue();
    }
}
