<?php
/**
 * @package Mellon
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

namespace BO\Mellon\Failure;

use BO\Mellon\Valid;

class Exception extends \Exception
{
    /**
     * @var Valid $validator
     */
    protected $validator = null;

    public function setValidator(Valid $validator): Exception
    {
        $this->validator = $validator;
        $this->message = ($validator->getMessages() ?? new MessageList())->__toString();
        $this->message .=
            "({"
            . $validator->getName()
            . "}=="
            . htmlspecialchars(escapeshellarg(substr((string)$validator->getUnvalidated(), 0, 65536)))
            . ")";
        return $this;
    }
}
