<?php
/**
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

namespace BO\Mellon;

/**
  * Validation of Strings
  * This validation is opinionated: It sanitizes the output from special chars for HTML
  */
class ValidPath extends ValidString
{
    /**
     * Check input, which is intended to be used in a file path
     *
     * @param string $message error message in case of failure
     *
     * @return self
     */
    public function isPath(string $message = 'no valid path'): ValidPath
    {
        if (!is_string($this->value)) {
            return $this->setFailure($message);
        }

        $this->validated = true;

        if (strlen($this->value) > 0) {
            $this->isShorterThan(65536, $message);
            if ($this->value != escapeshellcmd($this->value)) {
                return $this->setFailure($message);
            }
            $this->isFreeOf('#\.\.\/#', $message);
        }
        return $this->validate($message, FILTER_SANITIZE_SPECIAL_CHARS);
    }
}
